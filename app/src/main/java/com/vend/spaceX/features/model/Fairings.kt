package com.vend.spaceX.features.model

import java.io.Serializable

data class Fairings(val reused: Boolean?, val recovery_attempt: Boolean?, val recovered: Boolean?, val ship: String?): Serializable

