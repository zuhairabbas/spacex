package com.vend.spaceX.features.model

import java.io.Serializable

data class Orbit_params(val reference_system: String?, val regime: String?, val longitude: Any?, val semi_major_axis_km: Number?, val eccentricity: Number?, val periapsis_km: Number?, val apoapsis_km: Number?, val inclination_deg: Number?, val period_min: Number?, val lifespan_years: Any?, val epoch: String?, val mean_motion: Number?, val raan: Number?, val arg_of_pericenter: Number?, val mean_anomaly: Number?): Serializable

