package com.vend.spaceX.features.model

import java.io.Serializable

data class Rocket(val rocket_id: String?, val rocket_name: String?, val rocket_type: String?, val first_stage: First_stage?, val second_stage: Second_stage?, val fairings: Any?) : Serializable
