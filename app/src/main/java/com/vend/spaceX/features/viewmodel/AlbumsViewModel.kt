package com.vend.spaceX.features.viewmodel

import android.arch.lifecycle.LiveData
import com.vend.spaceX.core.BaseResponse
import com.vend.spaceX.core.BaseViewModel
import com.vend.spaceX.features.model.Album
import com.vend.spaceX.features.model.Base
import com.vend.spaceX.features.repository.AlbumRepo
import com.vend.spaceX.persistence.SharedPreferencesKeyValueDataSource
import javax.inject.Inject

class AlbumsViewModel @Inject constructor(private val repo: AlbumRepo, private val prefs: SharedPreferencesKeyValueDataSource) : BaseViewModel() {
    private lateinit var albums: LiveData<BaseResponse<List<Album>>>

    fun getAlbums(): LiveData<BaseResponse<List<Album>>> {

        if (!::albums.isInitialized) {
            albums = repo.getLiveAlbums()
        }

        return albums
    }

    fun getSpaceX() : LiveData<BaseResponse<List<Base>>>{
        return repo.getLiveSpaceXData()
    }

}