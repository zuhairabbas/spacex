package com.vend.spaceX.features

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProvider
import android.content.Context
import android.content.Intent
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.vend.spaceX.R
import com.vend.spaceX.core.BaseActivity
import com.vend.spaceX.core.DetailActivity
import com.vend.spaceX.core.Status
import com.vend.spaceX.databinding.ActivityAlbumsBinding
import com.vend.spaceX.features.model.Base
import com.vend.spaceX.features.viewmodel.AlbumsViewModel
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_albums.*
import javax.inject.Inject

class AlbumsActivity : BaseActivity<ActivityAlbumsBinding, AlbumsViewModel>(AlbumsViewModel::class.java) {

    @Inject
    lateinit var viewModelFactory: ViewModelProvider.Factory

    override fun getLayoutRes() = R.layout.activity_albums

    override fun getVMFactory(): ViewModelProvider.Factory {
        return viewModelFactory
    }

    override fun onInject() {
        AndroidInjection.inject(this)
    }

    override fun initViewModel(viewModel: AlbumsViewModel) {
        viewModel.getSpaceX().observe(this, Observer { albums ->
            albums?.let {
                when (it.status) {
                    Status.SUCCESS -> {
                        hideProgressBar()
                        inflateRecyclerView(it.data)
                    }
                    Status.ERROR -> {
                        hideProgressBar()
                        showError(it.message)
                    }

                    else -> {
                        showProgressBar()
                    }
                }
            } ?: kotlin.run {
                hideProgressBar()
            }


        })

    }

    private fun showError(message: String?) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    private fun showProgressBar() {
        progressBar.visibility = View.VISIBLE
    }

    private fun inflateRecyclerView(base: List<Base>?) {
        base?.let {
            recyclerView.layoutManager = LinearLayoutManager(this)
            recyclerView.adapter = BaseAdapter(it, object : Callback {
                override fun onItemClicked(base: Base) {
                    startActivity(DetailActivity.create(applicationContext, base))
                }
            })
        }
    }

    private fun hideProgressBar() {
        progressBar.visibility = View.GONE
    }

    interface Callback {
        fun onItemClicked(base: Base)
    }

    companion object {
        @JvmStatic
        fun create(context: Context): Intent{
            return Intent(context, AlbumsActivity::class.java)
        }
    }

}
