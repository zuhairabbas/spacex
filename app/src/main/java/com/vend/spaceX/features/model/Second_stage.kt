package com.vend.spaceX.features.model

import java.io.Serializable

data class Second_stage(val block: Number?, val payloads: List<Payloads>?): Serializable

