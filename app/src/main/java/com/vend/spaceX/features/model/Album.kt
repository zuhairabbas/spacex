package com.vend.spaceX.features.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity (tableName = "albums")
open class Album {
    var userId: Int = -1
    @PrimaryKey
    var id: Int = -1
    var title: String = ""
}