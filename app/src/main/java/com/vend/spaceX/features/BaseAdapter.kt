package com.vend.spaceX.features

import android.databinding.DataBindingUtil
import android.databinding.ViewDataBinding
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.TextView
import com.vend.spaceX.R
import com.vend.spaceX.databinding.ItemRecyclerBinding
import com.vend.spaceX.features.model.Base

class BaseAdapter(private var albums: List<Base>, var callBack: AlbumsActivity.Callback) : RecyclerView.Adapter<BaseAdapter.AlbumsViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AlbumsViewHolder {
        val binding: ViewDataBinding = DataBindingUtil.inflate(LayoutInflater.from(parent.context), R.layout.item_recycler, parent, false)
        return AlbumsViewHolder(binding, callBack)
    }


    override fun getItemCount(): Int {
        return albums.size
    }

    override fun onBindViewHolder(holder: AlbumsViewHolder, p1: Int) {
        holder.bind(albums[holder.adapterPosition])
        val countryName = albums[holder.adapterPosition].rocket?.second_stage?.payloads?.first()?.nationality
        holder.binding.root.findViewById<TextView>(R.id.tv_date_release).text = "Country Name: $countryName"

    }

    class AlbumsViewHolder(val binding: ViewDataBinding, var callBack: AlbumsActivity.Callback) : RecyclerView.ViewHolder(binding.root) {

        fun bind(base: Base) {
            (binding as ItemRecyclerBinding).base = base
            binding.executePendingBindings()

            binding.root.setOnClickListener { callBack.onItemClicked(base) }
        }

    }

}