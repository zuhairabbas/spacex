package com.vend.spaceX.features.model

import java.io.Serializable

data class Cores(val core_serial: String?, val flight: Number?, val block: Number?, val gridfins: Boolean?, val legs: Boolean?, val reused: Boolean?, val land_success: Boolean?, val landing_intent: Boolean?, val landing_type: String?, val landing_vehicle: String?) : Serializable
