package com.vend.spaceX.features.model

import java.io.Serializable

data class Telemetry(val flight_club: String?): Serializable

