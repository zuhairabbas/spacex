package com.vend.spaceX.features.model

import java.io.Serializable

data class Timeline(val webcast_liftoff: Number?, val go_for_prop_loading: Number?, val rp1_loading: Number?, val stage1_lox_loading: Number?, val stage2_lox_loading: Number?, val engine_chill: Number?, val prelaunch_checks: Number?, val propellant_pressurization: Number?, val go_for_launch: Number?, val ignition: Number?, val liftoff: Number?, val maxq: Number?, val meco: Number?, val stage_sep: Number?, val second_stage_ignition: Number?, val first_stage_entry_burn: Number?, val seco: Number?,
                    val first_stage_landing_burn: Number?, val first_stage_landing: Number?, val dragon_separation: Number?): Serializable
