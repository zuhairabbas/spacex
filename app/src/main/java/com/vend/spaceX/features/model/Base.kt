package com.vend.spaceX.features.model

import java.io.Serializable

data class Base(val flight_number: Number?, val mission_name: String?, val mission_id: List<String>?, val launch_year: String?, val launch_date_unix: Number?, val launch_date_utc: String?, val launch_date_local: String?, val is_tentative: Boolean?, val tentative_max_precision: String?, val tbd: Boolean?, val launch_window: Number?, val rocket: Rocket?, val ships: List<String>?, val telemetry: Telemetry?, val launch_site: Launch_site?, val launch_success: Boolean?, val links: Links?, val details: String?, val upcoming: Boolean?, val static_fire_date_utc: String?, val static_fire_date_unix: Number?, val timeline: Timeline?): Serializable

