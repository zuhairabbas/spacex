package com.vend.spaceX.features.model

import java.io.Serializable

data class Launch_failure_details(val time: Number?, val altitude: Number?, val reason: String?): Serializable

