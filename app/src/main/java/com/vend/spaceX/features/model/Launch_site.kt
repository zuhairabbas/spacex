package com.vend.spaceX.features.model

import java.io.Serializable

data class Launch_site(val site_id: String?, val site_name: String?, val site_name_long: String?): Serializable

