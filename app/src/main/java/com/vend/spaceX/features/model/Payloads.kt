package com.vend.spaceX.features.model

import java.io.Serializable

data class Payloads(val payload_id: String?, val norad_id: List<Number>?, val cap_serial: String?, val reused: Boolean?, val customers: List<String>?, val nationality: String?, val manufacturer: String?, val payload_type: String?, val payload_mass_kg: Number?, val payload_mass_lbs: Number?, val orbit: String?, val orbit_params: Orbit_params?, val mass_returned_kg: Any?, val mass_returned_lbs: Any?, val flight_time_sec: Any?, val cargo_manifest: Any?): Serializable

