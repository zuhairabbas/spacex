package com.vend.spaceX.features.repository

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MediatorLiveData
import com.google.gson.reflect.TypeToken
import com.vend.spaceX.apicalls.ServiceGateway
import com.vend.spaceX.backend.NetworkHelper
import com.vend.spaceX.core.BaseResponse
import com.vend.spaceX.features.model.Album
import com.vend.spaceX.features.model.AlbumDAO
import com.vend.spaceX.features.model.Base
import com.vend.spaceX.persistence.SharedPreferencesKeyValueDataSource
import io.reactivex.Completable
import io.reactivex.schedulers.Schedulers
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
class AlbumRepo @Inject constructor(private val serviceGateway: ServiceGateway,
                                    private val dao: AlbumDAO,
                                    private val networkHelper: NetworkHelper,
                                    private val sharedPreferencesKeyValueDataSource: SharedPreferencesKeyValueDataSource) {

    var SPACEX_DATA = "SPACEX_DATA"

    private val liveAlbum = MediatorLiveData<BaseResponse<List<Album>>>()

    private val liveSpaceX = MediatorLiveData<BaseResponse<List<Base>>>()

    fun getLiveAlbums(): LiveData<BaseResponse<List<Album>>> {

        val dbSource = dao.getAll()
        liveAlbum.addSource(dbSource) { albums ->
            if (!albums.isNullOrEmpty()) {
                liveAlbum.value = BaseResponse.success(albums)
            } else {
                fetchFromNetwork()
            }
        }

        return liveAlbum
    }

    private fun fetchFromNetwork() {

        val networkSource = networkHelper.serviceCall(serviceGateway.getAlbums())

        liveAlbum.addSource(networkSource) { response ->
            response?.data?.let {
                liveAlbum.removeSource(networkSource)
                Completable.fromCallable { dao.insert(*it.toTypedArray()) }
                        .subscribeOn(Schedulers.newThread())
                        .subscribe()
            } ?: kotlin.run {
                liveAlbum.value = response
            }

        }
    }

    fun getLiveSpaceXData(): LiveData<BaseResponse<List<Base>>> {

        var dbSource : List<Base>? = null
        dbSource = sharedPreferencesKeyValueDataSource.getModelValue(SPACEX_DATA,
                object : TypeToken<List<Base>>() {}.type, null)

        if(dbSource != null){
            liveSpaceX.value = BaseResponse.success(dbSource)
        }else{
            fetchFromNetworkV2()
        }


        return liveSpaceX
    }

    fun fetchFromNetworkV2():  LiveData<BaseResponse<List<Base>>>{

        val networkSource = networkHelper.serviceCall(serviceGateway.getSpaceXData())
        liveSpaceX.addSource(networkSource) { response ->
            response?.data?.let {
                sharedPreferencesKeyValueDataSource.storeModelValue(SPACEX_DATA, it)
              liveSpaceX.value = BaseResponse.success(it)
            } ?: kotlin.run {
                liveSpaceX.value = response
            }
        }
        return liveSpaceX
    }
}