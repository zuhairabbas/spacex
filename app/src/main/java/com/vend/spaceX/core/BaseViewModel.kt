package com.vend.spaceX.core

import android.arch.lifecycle.ViewModel

open class BaseViewModel() : ViewModel()