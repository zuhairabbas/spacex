package com.vend.spaceX.core

import android.animation.Animator
import android.animation.AnimatorListenerAdapter
import android.animation.ValueAnimator
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.util.DisplayMetrics
import android.view.View
import android.view.animation.LinearInterpolator
import com.vend.spaceX.R
import com.vend.spaceX.features.AlbumsActivity


class SplashActivity : AppCompatActivity() {
    protected lateinit var rocket: View
    protected var screenHeight = 0f

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        setContentView(R.layout.splash_activity)

        rocket = findViewById(R.id.rocket)
    }

    override fun onResume() {
        super.onResume()

        val displayMetrics = DisplayMetrics()
        windowManager.defaultDisplay.getMetrics(displayMetrics)
        screenHeight = displayMetrics.heightPixels.toFloat()
        onStartAnimation()
    }

    private fun onStartAnimation() {
        val valueAnimator = ValueAnimator.ofFloat(0f, -screenHeight)

        valueAnimator.addUpdateListener {
            val value = it.animatedValue as Float
            rocket.translationY = value
        }
        valueAnimator.addListener(object : AnimatorListenerAdapter() {
            override fun onAnimationEnd(animation: Animator) {
                startActivity(AlbumsActivity.create(applicationContext))
            }
        })
        valueAnimator.interpolator = LinearInterpolator()
        valueAnimator.duration = SplashActivity.DEFAULT_ANIMATION_DURATION
        valueAnimator.start()
    }

    companion object {
        const val DEFAULT_ANIMATION_DURATION = 3000L
    }
}
