package com.vend.spaceX.core

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast
import com.bumptech.glide.Glide
import com.vend.spaceX.features.model.Base

import kotlinx.android.synthetic.main.activity_detail.*
import com.bumptech.glide.request.RequestOptions
import com.vend.spaceX.R


class DetailActivity : AppCompatActivity() {

    var base: Base? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_detail)

        intent?.let {
            base = it.extras?.getSerializable(key) as Base
        }

        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.holder)
        requestOptions.error(R.drawable.holder)


        base?.let {
            name.text = "Mission name: "+ it.mission_name
            Glide.with(this)
                    .setDefaultRequestOptions(requestOptions)
                    .load(it.links?.mission_patch)
                    .into(picture)
            name2.text = "Launch year: " + it.launch_year
            name3.text = "Flight Number: " + it.flight_number

        }?:run{
            Toast.makeText(this, "Could not read from the data passed", Toast.LENGTH_SHORT).show()
        }

    }


    companion object {
        val key = "KEY"
        @JvmStatic
        fun create(context: Context, base: Base): Intent{
            val detailActivity = Intent(context, DetailActivity::class.java)
            detailActivity.putExtra(key, base)
            return detailActivity
        }
    }
}
