package com.vend.spaceX

import com.vend.spaceX.di.DaggerAppComponent
import dagger.android.AndroidInjector
import dagger.android.DaggerApplication

class SpaceXApplication: DaggerApplication() {

    companion object {
        private lateinit var sInstance: SpaceXApplication
        fun getInstance(): SpaceXApplication {
            return sInstance
        }
    }

    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent
                .builder()
                .create(this)
    }

    override fun onCreate() {
        super.onCreate()
        sInstance = this
    }
}