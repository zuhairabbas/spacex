package com.vend.spaceX.di.module

import com.vend.spaceX.features.AlbumsActivity
import com.vend.spaceX.di.annotations.ActivityScope
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivityBuilder {

    @ActivityScope
    @ContributesAndroidInjector(modules = [(FragmentBuilder::class)])
    internal abstract fun bindAlbumActivity(): AlbumsActivity
}
