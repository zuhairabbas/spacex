package com.vend.spaceX.di.module

import android.app.Application
import android.arch.persistence.room.Room
import android.content.Context
import android.content.SharedPreferences
import com.vend.spaceX.SpaceXApplication
import com.vend.spaceX.features.model.AlbumDAO
import com.vend.spaceX.persistence.LocalDB
import dagger.Module
import dagger.Provides
import javax.inject.Singleton


@Module(includes = [ViewModelModule::class])
class AppModule {

    @Provides
    @Singleton
    fun provideApplicationContext(application: SpaceXApplication): Context {
        return application.applicationContext
    }

    @Provides
    @Singleton
    fun provideApplication(application: SpaceXApplication): Application {
        return application
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }


    @Singleton
    @Provides
    fun provideDb(application: Application): LocalDB {
        return Room.databaseBuilder(application, LocalDB::class.java, "dnow-db").build()

    }

    @Singleton
    @Provides
    fun provideMoviesDao(database: LocalDB): AlbumDAO {
        return database.getAlbumDao()
    }

}
