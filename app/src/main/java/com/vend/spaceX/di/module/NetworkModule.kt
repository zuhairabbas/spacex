package com.vend.spaceX.di.module

import com.google.gson.Gson
import com.google.gson.GsonBuilder
import com.vend.spaceX.BuildConfig
import com.vend.spaceX.apicalls.ServiceGateway
import com.vend.spaceX.utils.Constants
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import javax.inject.Named
import javax.inject.Singleton


@Module
class NetworkModule {

    @Named("BASE_URL")
    @Provides
    fun getBaseUrl(): String {
        return Constants.BASE_URL
    }

    @Provides
    @Singleton
    fun provideGson() = GsonBuilder().setLenient().create()

    @Provides
    @Singleton
    fun provideBaseOkHttpClient(): OkHttpClient {
        val httpClientBuilder = OkHttpClient.Builder()
        val httpLoggingInterceptor = HttpLoggingInterceptor()
                .setLevel(if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE)

        if (BuildConfig.DEBUG) {
            val interceptor = HttpLoggingInterceptor()
            interceptor.level = HttpLoggingInterceptor.Level.BODY
            httpClientBuilder.addInterceptor(interceptor)

        }

        return httpClientBuilder.addInterceptor(httpLoggingInterceptor)
                .build()
    }

    @Provides
    @Singleton
    fun provideRetrofitBuilder(): Retrofit.Builder {
        return Retrofit.Builder()
                .addConverterFactory(GsonConverterFactory.create(Gson()))
                .addCallAdapterFactory(RxJava2CallAdapterFactory.createAsync())
    }

    @Provides
    @Singleton
    open fun provideCoreGateway(
            baseClient: OkHttpClient,
            retrofitBuilder: Retrofit.Builder
    ): ServiceGateway {
        return retrofitBuilder
                .baseUrl(Constants.BASE_URL)
                .client(baseClient)
                .build()
                .create(ServiceGateway::class.java)
    }

}
