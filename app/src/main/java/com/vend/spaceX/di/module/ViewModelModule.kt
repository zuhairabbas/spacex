package com.vend.spaceX.di.module

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import com.vend.spaceX.di.ViewModelFactory
import com.vend.spaceX.di.annotations.ViewModelKey
import com.vend.spaceX.features.viewmodel.AlbumsViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap


@Module
abstract class ViewModelModule {

    @Binds
    internal abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(AlbumsViewModel::class)
    internal abstract fun mainViewModel(viewModel: AlbumsViewModel): ViewModel

}