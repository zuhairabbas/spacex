package com.vend.spaceX.di

import com.vend.spaceX.SpaceXApplication
import com.vend.spaceX.di.module.ActivityBuilder
import com.vend.spaceX.di.module.AppModule
import com.vend.spaceX.di.module.NetworkModule
import dagger.Component
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(modules = [AppModule::class,
    AndroidSupportInjectionModule::class,
    NetworkModule::class, ActivityBuilder::class])

interface AppComponent : AndroidInjector<SpaceXApplication> {
    @Component.Builder
    abstract class Builder : AndroidInjector.Builder<SpaceXApplication>()
}
