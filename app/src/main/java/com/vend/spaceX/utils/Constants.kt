package com.vend.spaceX.utils

class Constants {

    companion object {
        val BASE_URL = "https://api.spacexdata.com/v3/launches/"
    }
}