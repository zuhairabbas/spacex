package com.vend.spaceX.persistence

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.vend.spaceX.features.model.Album
import com.vend.spaceX.features.model.AlbumDAO

@Database(entities = [Album::class], version = 1, exportSchema = false)
abstract class LocalDB:RoomDatabase() {

    abstract fun getAlbumDao(): AlbumDAO
}