package com.vend.spaceX.persistence

import android.content.SharedPreferences
import com.google.gson.Gson
import java.lang.reflect.Type
import javax.inject.Inject

class SharedPreferencesKeyValueDataSource
@Inject constructor(var sharedPreferences: SharedPreferences) {

    open fun setValue(key: String, value: Any) {
        sharedPreferences[key] = value
    }

    inline open fun <reified T : Any> getValue(key: String, value: T? = null): T? {
        return sharedPreferences[key, value]
    }

    fun <T> storeModelValue(key: String, value: T) {
        sharedPreferences[key] = Gson().toJson(value)
    }

    fun <T> getModelValue(key: String, classType: Type, defaultValue: T?): T? {
        val jsonValue = sharedPreferences[key, ""]

        return if (!jsonValue.isNullOrEmpty()) {
            Gson().fromJson<T>(jsonValue, classType)
        } else {
            defaultValue
        }
    }

}
