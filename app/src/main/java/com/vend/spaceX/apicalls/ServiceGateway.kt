package com.vend.spaceX.apicalls

import com.vend.spaceX.features.model.Album
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import com.vend.spaceX.features.model.Base


interface ServiceGateway {

    @GET("albums")
    fun getAlbums(): Single<Response<List<Album>>>

    @GET("")
    fun getSpaceXData(): Single<Response<List<Base>>>

}
